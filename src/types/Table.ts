export default interface Table {
  id?: number;
  maxSeat: number;
  status: string;
  qrCodeSource: string;
}
