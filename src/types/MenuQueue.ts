import type Menu from "./Menu";
import type OrderItems from "./OrderItems";

export default interface MenuQueue {
  id: number;
  menuStatus?: string;
  menu?: Menu;
  orderItem?: OrderItems;
  createAt?: Date;
}
