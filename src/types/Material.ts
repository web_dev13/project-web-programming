export default interface Material {
  id?: number;
  category: string;
  name: string;
  minimum: number;
  onhand: number;
  unit: string;
}
