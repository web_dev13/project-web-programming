export default interface Menu {
  id?: number;
  name: string;
  imgSrc: string;
  category: string;
  descript: string;
  size: string;
  price: number;
  status: string;
  qty: number;
}
