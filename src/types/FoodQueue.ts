export default interface FoodQueue {
  orderNumber: number;
  tableNumber: number;
  customerName: string;
  foodList: string[];
  dateTime: string;
  orderStatus: string;
}
