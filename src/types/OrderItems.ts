import type Table from "./Table";
import type Orders from "./Orders";
import type Menu from "./Menu";
import type MenuQueue from "./MenuQueue";

export default interface OrderItems {
  id?: number;
  // order: Orders;
  menu?: Menu;
  status?: string;
  quantity?: number;
  price?: number;
  total?: number;
  menuQueues?: MenuQueue[];
  order?: Orders;
}
