import type AttendanceReport from "./AttendanceReport";
import type Employee from "./Employee";

export default interface AttendanceDetail {
  id?: number;
  employee: Employee;
  workStart: string;
  workOff: string;
  wage: number;
  status: string;
  attendanceReport?: AttendanceReport;
}
