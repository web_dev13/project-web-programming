import type Orders from "./Orders";

export default interface ShowOrder {
  id: number;
  table: string;
  name: string[];
  price: number[];
  setSize: string[];
  qty: number[];
}
