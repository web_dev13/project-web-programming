export default interface Stock {
  id?: number;
  categories: string;
  name: string;
  minimum: number;
  onHand: number;
  unit: string;
}
