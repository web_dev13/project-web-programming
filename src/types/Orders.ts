import type Employee from "./Employee";
import type OrderItems from "./OrderItems";
import type Table from "./Table";

export default interface Orders {
  id?: number;
  dateTime?: Date;
  storeName?: string;
  status?: string;
  tax?: string;
  subTotal?: number;
  total?: number;
  paymentType?: string;
  cash?: number;
  change?: number;
  table?: Table;
  employee?: Employee;
  orderItems?: OrderItems[];
}
