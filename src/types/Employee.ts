export default interface Employee {
  id?: number;
  name: string;
  surname: string;
  age: number;
  tel: string;
  role: string;
  username: string;
  password?: string;
}
