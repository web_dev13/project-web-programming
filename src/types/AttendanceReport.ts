import type AttendanceDetail from "./AttendanceDetail";

export default interface AttendanceReport {
  id?: number;
  total: number;
  payDate: string;
  attendanceDetails?: AttendanceDetail[];
}
