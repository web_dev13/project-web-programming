import { ref } from "vue";
import { defineStore } from "pinia";
import type Table from "@/types/Table";
import tableService from "@/service/table";
import { useMessageStore } from "./message";
import { useLoadingStore } from "./loading";

export const useTableStore = defineStore("Table", () => {
  const loadingStore = useLoadingStore();
  const messageStore = useMessageStore();
  const tables = ref<Table[]>([]);
  const showOpenDialog = ref(false);
  const editTableDialog = ref(false);
  const deleteTableDialog = ref(false);
  const editedTable = ref<Table>({
    maxSeat: 0,
    status: "Available",
    qrCodeSource: "",
  });
  const editedStatus = ref("");

  async function getTables() {
    loadingStore.showWithMessage("Syncing Data");
    try {
      tables.value = (await tableService.getTables()).data;
    } catch (exception) {
      loadingStore.closeDialog(0);
      // Ignore these error. The code work just fine.
      messageStore.showError("Syncing Data Failed!", exception.message);
    }
    loadingStore.closeDialog(0);
  }

  function changeTableStatus(table: Table, status: string) {
    editedTable.value = { ...table };
    if (status == "Avaliable") {
      editedStatus.value = status;
      saveTable();
    } else {
      editedStatus.value = status;
      editTableDialog.value = true;
    }
  }

  async function saveTable() {
    editTableDialog.value = false;
    loadingStore.showWithMessage(
      editedTable.value.id ? "Saving in Progress" : "Adding in Progress"
    );
    try {
      if (editedTable.value.id) {
        await tableService.updateTable(editedTable.value.id, editedTable.value);
      } else {
        await tableService.saveTable(editedTable.value);
      }
      await getTables();
      resetEditedTable();
    } catch (exception) {
      loadingStore.closeDialog(0);
      // Ignore these error. The code work just fine.
      messageStore.showError(
        "Request Failed!",
        exception.response.data.message,
        exception.response.status
      );
    }
    loadingStore.closeDialog(0);
  }

  async function saveTableByStatus(stt: string) {
    showOpenDialog.value = false;
    loadingStore.showWithMessage("Processing");
    try {
      editedTable.value.status = stt;
      await tableService.updateTable(editedTable.value.id!, editedTable.value);
      await getTables();
      resetEditedTable();
    } catch (exception) {
      loadingStore.closeDialog(0);
      // Ignore these error. The code work just fine.
      messageStore.showError(
        "Request Failed!",
        exception.response.data.message,
        exception.response.status
      );
    }
    loadingStore.closeDialog(0);
  }

  async function deleteTable() {
    deleteTableDialog.value = false;
    loadingStore.showWithMessage("Deleting in Progress");
    try {
      await tableService.deleteTable(editedTable.value.id!);
      await getTables();
      resetEditedTable();
    } catch (exception) {
      loadingStore.closeDialog(0);
      // Ignore these error. The code work just fine.
      messageStore.showError(
        "Request Failed!",
        exception.response.data.message,
        exception.response.status
      );
    }
    loadingStore.closeDialog(0);
  }

  async function resetEditedTable() {
    await new Promise((resolve) => setTimeout(resolve, 100));
    editedTable.value = {
      maxSeat: 0,
      status: "Available",
      qrCodeSource: "",
    };
  }

  return {
    showOpenDialog,
    tables,
    getTables,
    editTableDialog,
    deleteTableDialog,
    changeTableStatus,
    saveTable,
    editedTable,
    resetEditedTable,
    deleteTable,
    saveTableByStatus,
  };
});
