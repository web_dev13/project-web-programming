import { ref } from "vue";
import { defineStore } from "pinia";
import type AttendanceDetail from "@/types/AttendanceDetail";
import attendanceDetailService from "@/service/attendanceDetail";
import attendanceReportService from "@/service/attendanceReport";
import { useMessageStore } from "./message";
import { useLoadingStore } from "./loading";
import type AttendanceReport from "@/types/AttendanceReport";

export const useAtdStore = defineStore("attendance", () => {
  const loadingStore = useLoadingStore();
  const messageStore = useMessageStore();
  const searchId = ref(0);
  const attendanceDetailsDialog = ref(false);
  const dailyAttendanceDetails = ref<AttendanceDetail[]>([]);
  const attendanceDetails = ref<AttendanceDetail[]>([]);
  const attendanceReports = ref<AttendanceReport[]>([]);
  const attendanceReportById = ref<AttendanceReport>({
    payDate: "",
    total: 0,
  });
  const selectedAttendanceReport = ref<AttendanceDetail[]>([]);
  const editedAttendaceDetail = ref<AttendanceDetail>({
    employee: {
      username: "",
      age: 0,
      name: "",
      surname: "",
      role: "",
      tel: "",
    },
    status: "",
    wage: 0,
    workOff: "",
    workStart: "",
  });
  const editedAttendaceReport = ref<AttendanceReport>({
    payDate: "",
    total: 0,
  });
  const wageRate = ref(11.74);

  function getCurrentDateTime() {
    const currentDate = new Date();
    const logginDatetime = ref("");
    // Ignore these error. The code work just fine.
    const day = String(currentDate.getDate()).padStart(2, "0");
    const month = String(currentDate.getMonth() + 1).padStart(2, "0");
    const year = currentDate.getFullYear();
    logginDatetime.value = `${day}-${month}-${year}`;
    return logginDatetime.value;
  }

  async function addToDailyAttendanceDetails(item: AttendanceDetail) {
    const tempReport = ref<AttendanceReport>({
      payDate: "",
      total: 0,
    });
    const res = await attendanceReportService.getAttendanceReportByDate(
      getCurrentDateTime()
    );
    tempReport.value = res.data;
    item.attendanceReport = tempReport.value;
    editedAttendaceDetail.value = item;
    saveAttendaceDetail();
    editedAttendaceReport.value = tempReport.value;
    if (editedAttendaceDetail.value.employee.role == "พนักงาน") {
      editedAttendaceReport.value.total += editedAttendaceDetail.value.wage;
    }
    saveAttendanceReport();
  }

  async function getAttendaceDetails() {
    loadingStore.showWithMessage("Syncing Data");
    try {
      const res = await attendanceDetailService.getAttendanceDetails();
      attendanceDetails.value = res.data;
    } catch (exception) {
      loadingStore.closeDialog(0);
      // Ignore these error. The code work just fine.
      messageStore.showError("Syncing Data Failed!", exception.message);
    }
    loadingStore.closeDialog(0);
  }

  async function getAttendanceReports() {
    loadingStore.showWithMessage("Syncing Data");
    try {
      const res = await attendanceReportService.getAttendanceReports();
      attendanceReports.value = res.data;
    } catch (exception) {
      loadingStore.closeDialog(0);
      // Ignore these error. The code work just fine.
      messageStore.showError("Syncing Data Failed!", exception.message);
    }
    loadingStore.closeDialog(0);
  }

  async function getAttendanceReportById(id: number) {
    loadingStore.showWithMessage("Syncing Data");
    try {
      const res = await attendanceReportService.getAttendanceReportById(id);
      attendanceReportById.value = res.data;
      selectedAttendanceReport.value =
        attendanceReportById.value.attendanceDetails!;
    } catch (exception) {
      loadingStore.closeDialog(0);
      // Ignore these error. The code work just fine.
      messageStore.showError("Syncing Data Failed!", exception.message);
    }
    loadingStore.closeDialog(0);
  }

  async function saveAttendaceDetail() {
    loadingStore.showWithMessage("Saving in Progress");
    try {
      if (editedAttendaceDetail.value.employee.role == "พนักงาน") {
        if (editedAttendaceDetail.value.id) {
          await attendanceDetailService.updateAttendanceDetail(
            editedAttendaceDetail.value.id,
            editedAttendaceDetail.value
          );
        } else {
          await attendanceDetailService.saveAttendanceDetail(
            editedAttendaceDetail.value
          );
        }
      }
    } catch (exception) {
      // Ignore these error. The code work just fine.
      loadingStore.closeDialog(0);
      messageStore.showError(
        "Request Failed!",
        exception.response.data.message,
        exception.response.status
      );
    }
    await getAttendanceReportById(searchId.value);
    await getAttendaceDetails();
    await getAttendanceReports();
    resetAttendaceDetail();
    loadingStore.closeDialog(0);
  }

  async function saveAttendanceReport() {
    loadingStore.showWithMessage("Saving in Progress");
    try {
      if (editedAttendaceReport.value.id) {
        await attendanceReportService.updateAttendanceReport(
          editedAttendaceReport.value.id,
          editedAttendaceReport.value
        );
      } else {
        await attendanceReportService.saveAttendanceReport(
          editedAttendaceReport.value
        );
      }
    } catch (exception) {
      loadingStore.closeDialog(0);
      // Ignore these error. The code work just fine.
      messageStore.showError(
        "Request Failed!",
        exception.response.data.message,
        exception.response.status
      );
    }
    await getAttendanceReportById(searchId.value);
    await getAttendaceDetails();
    await getAttendanceReports();
    resetAttendaceDetail();
    loadingStore.closeDialog(0);
  }

  function setEditedAttendaceDetail(atd: AttendanceDetail, stt: string) {
    editedAttendaceDetail.value = atd;
    editedAttendaceDetail.value.status = stt;
    saveAttendaceDetail();
  }

  function delay(ms: number) {
    return new Promise((resolve) => setTimeout(resolve, ms));
  }

  async function resetAttendaceDetail() {
    await delay(100);
    editedAttendaceDetail.value = {
      employee: {
        username: "",
        age: 0,
        name: "",
        surname: "",
        role: "",
        tel: "",
      },
      status: "",
      wage: 0,
      workOff: "",
      workStart: "",
    };
  }

  return {
    addToDailyAttendanceDetails,
    searchId,
    attendanceDetailsDialog,
    getAttendaceDetails,
    getAttendanceReports,
    getAttendanceReportById,
    attendanceDetails,
    attendanceReports,
    attendanceReportById,
    editedAttendaceDetail,
    editedAttendaceReport,
    setEditedAttendaceDetail,
    saveAttendaceDetail,
    saveAttendanceReport,
    selectedAttendanceReport,
    getCurrentDateTime,
    wageRate,
  };
});
