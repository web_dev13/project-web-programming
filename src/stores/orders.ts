import { ref } from "vue";
import { defineStore } from "pinia";
import type Orders from "@/types/Orders";
import orderService from "@/service/order";
import { useMessageStore } from "./message";
import { useLoadingStore } from "./loading";

export const useOrdersStore = defineStore("orders", () => {
  const loadingStore = useLoadingStore();
  const messageStore = useMessageStore();
  const showDeleteDialog = ref(false);
  const showEditDialog = ref(false);
  const showAddDialog = ref(false);
  const editedOrder = ref<Orders>({
    dateTime: new Date(),
    storeName: "OOADFood",
    status: "In kitchen",
    tax: "7",
    subTotal: 0,
    total: 0,
    paymentType: "",
    cash: 0,
    change: 0,
    table: { maxSeat: 0, status: "", qrCodeSource: "" },
    employee: {
      age: 0,
      name: "",
      surname: "",
      tel: "",
      role: "พนักงาน",
      username: "",
    },
  });
  const orders = ref<Orders[]>([]);

  const editorders = (orders: Orders) => {
    editedOrder.value = { ...orders };
    showEditDialog.value = true;
  };

  const openOrder = async (
    tableId: number,
    empId: number,
    odItem: { quantity: number; menuId: number }[]
  ) => {
    // localStorage.removeItem("oldOrderID");
    const oldId = localStorage.getItem("oldOrderID");
    const order = {
      storeName: "OOADFood",
      status: "In process",
      tax: 7,
      paymentType: "none",
      cash: 0,
      change: 0,
      tableId: tableId,
      employeeId: empId,
      orderItems: odItem,
    };
    console.log(oldId);
    if (!oldId) {
      try {
        console.log(order);
        const res = await orderService.saveOrder(order);
        localStorage.setItem("oldOrderID", JSON.stringify(res.data.id));
        console.log(res);
      } catch (error) {
        console.log(error);
      }
    } else {
      const tempOldId = JSON.parse(oldId);
      const tempOrder = await getSingleOrder(tempOldId);
      console.log(tempOrder);

      if (tempOrder.status === "Complete" || tempOrder.status === "Cancel") {
        localStorage.removeItem("oldOrderID");
        const order = {
          storeName: "OOADFood",
          status: "In kitchen",
          tax: 7,
          paymentType: "none",
          cash: 0,
          change: 0,
          tableId: tableId,
          employeeId: empId,
          orderItems: odItem,
        };
        try {
          console.log(order);
          const res = await orderService.saveOrder(order);
          localStorage.setItem("oldOrderID", JSON.stringify(res.data.id));
          console.log(res);
        } catch (error) {
          console.log(error);
        }
      } else {
        const order = <Orders>{ orderItems: odItem };
        try {
          console.log(order);
          const res = await orderService.updateOrder(tempOldId, order);
          localStorage.setItem("oldOrderID", JSON.stringify(res.data.id));
          console.log(res);
        } catch (error) {
          console.log(error);
        }
      }
    }
  };

  async function deleteOrder() {
    showDeleteDialog.value = false;
    loadingStore.showWithMessage("Deleting in Progress");
    try {
      await orderService.deleteOrder(editedOrder.value.id!);
    } catch (exception) {
      loadingStore.isLoading = false;
      // Ignore these error. The code work just fine.
      messageStore.showError(
        "Request Failed!",
        exception.response.data.message,
        exception.response.status
      );
    }
    await getOrders();
    resetEditedOrder();
    loadingStore.isLoading = false;
  }

  const getOrders = async () => {
    loadingStore.showWithMessage("Syncing Data");
    try {
      const res = await orderService.getOrders();
      orders.value = res.data;
    } catch (exception) {
      loadingStore.closeDialog(0);
      // Ignore these error. The code work just fine.
      messageStore.showError("Syncing Data Failed!", exception.message);
    }
    loadingStore.closeDialog(0);
  };
  const reloadOrders = async () => {
    try {
      const res = await orderService.getOrders();
      orders.value = res.data;
    } catch (exception) {
      loadingStore.closeDialog(0);
      // Ignore these error. The code work just fine.
      messageStore.showError("Syncing Data Failed!", exception.message);
    }
  };

  const getSingleOrder = async (id: number) => {
    try {
      const res = await orderService.getOrder(id);
      // orders.value = res.data;
      return res.data;
    } catch (error) {
      console.log(error);
    }
  };

  const resetEditedOrder = async () => {
    await new Promise((resolve) => setTimeout(resolve, 100));
    editedOrder.value = {
      dateTime: new Date(),
      storeName: "",
      status: "",
      tax: "",
      subTotal: 0,
      total: 0,
      paymentType: "",
      cash: 0,
      change: 0,
      table: { id: -1, maxSeat: 0, status: "", qrCodeSource: "" },
      employee: {
        age: 0,
        name: "",
        surname: "",
        tel: "",
        role: "พนักงาน",
        username: "",
      },
    };
  };
  const updateOrderPay = async (id: number, paymentType: string) => {
    loadingStore.showWithMessage("Syncing Data");
    try {
      if (id) {
        const order = { paymentType: paymentType, status: "Complete" };
        // console.log(order);
        const res = await orderService.updateOrderPay(id, order);
        console.log(res.data);
        loadingStore.closeDialog(0);
        return res.data;
      }
      // orders.value = res.data;
    } catch (error) {
      console.log(error);
    }
    localStorage.removeItem("oldOrderID");
    await getOrders();
    loadingStore.closeDialog(0);
  };

  return {
    orders,
    deleteOrder,
    showDeleteDialog,
    showEditDialog,
    showAddDialog,
    openOrder,
    resetEditedOrder,
    editorders,
    editedOrder,
    getOrders,
    getSingleOrder,
    updateOrderPay,
    reloadOrders,
  };
});
