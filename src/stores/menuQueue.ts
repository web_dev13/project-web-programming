import { ref } from "vue";
import { defineStore } from "pinia";
import type MenuQueue from "@/types/MenuQueue";
import { useLoadingStore } from "./loading";
import menuQueueService from "@/service/menuQueue";
import type OrderItems from "@/types/OrderItems";

export const useMenuQueueStore = defineStore("menuQueue", () => {
  const loadingStore = useLoadingStore();
  const selectedId = ref(0);
  const action = ref("");
  const deleteOrderId = ref(-1);

  const showDialog = ref(false);

  // Mockup Data
  const menuQueues = ref<MenuQueue[]>([]);

  const getMenuQueue = async () => {
    loadingStore.showWithMessage("Syncing Data");
    try {
      const res = await menuQueueService.getMenuQueue();
      menuQueues.value = res.data;
      loadingStore.closeDialog(0);
      return res.data;
    } catch (error) {
      console.log(error);
      loadingStore.closeDialog(0);
    }
    loadingStore.closeDialog(0);
  };

  const reloadmenuQueue = async () => {
    try {
      const res = await menuQueueService.getMenuQueue();
      menuQueues.value = res.data;
    } catch (error) {
      console.log(error);
    }
  };
  const orderItem = ref<OrderItems>({});
  const getMenuQueueWithODItem = async (odItemId: number) => {
    try {
      const res = await menuQueueService.getMenuQueueWithODItem(odItemId);
      console.log(res.data);
      orderItem.value = res.data;
      return res.data;
    } catch (error) {
      console.log(error);
    }
  };
  const denyQueue = async (id: number): Promise<void> => {
    loadingStore.showWithMessage("Syncing Data");

    const menuQueue = <MenuQueue>{ menuStatus: "Cancel" };
    try {
      await menuQueueService.updateMenuQueue(id, menuQueue);
    } catch (error) {
      console.log(error);
    }
    await getMenuQueue();
  };

  const acceptQueue = async (id: number) => {
    loadingStore.showWithMessage("Syncing Data");
    const menuQueue = <MenuQueue>{ menuStatus: "Kitchen" };
    try {
      await menuQueueService.updateMenuQueue(id, menuQueue);
    } catch (error) {
      console.log(error);
    }
    await getMenuQueue();
  };

  const finishQueue = async (id: number) => {
    loadingStore.showWithMessage("Syncing Data");
    const menuQueue = <MenuQueue>{ menuStatus: "Ready to serve" };
    try {
      await menuQueueService.updateMenuQueue(id, menuQueue);
    } catch (error) {
      console.log(error);
    }

    await getMenuQueue();
  };
  const serveQueue = async (checkedID: number[]) => {
    loadingStore.showWithMessage("Syncing Data");
    const menuQueue = <MenuQueue>{ menuStatus: "Served" };
    const updatePromises = checkedID.map((id) => {
      return menuQueueService.updateMenuQueue(id, menuQueue);
    });
    await Promise.all(updatePromises);
    const newMenuQueues = await getMenuQueue();
    menuQueues.value.splice(0, menuQueues.value.length, ...newMenuQueues);
    // console.log(menuQueues.value);
  };

  const confirm = (id: number) => {
    showDialog.value = true;
    deleteOrderId.value = id;
  };

  return {
    action,
    selectedId,
    acceptQueue,
    menuQueues,
    denyQueue,
    showDialog,
    confirm,
    deleteOrderId,
    getMenuQueue,
    finishQueue,
    serveQueue,
    getMenuQueueWithODItem,
    reloadmenuQueue,
  };
});
