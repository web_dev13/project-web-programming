import { defineStore } from "pinia";
import auth from "@/service/auth";
import { useLoadingStore } from "./loading";
import { useMessageStore } from "./message";
import { useAtdStore } from "@/stores/attendance";
import router from "@/router";
import { ref } from "vue";
import type Employee from "@/types/Employee";
import type Attendance from "@/types/AttendanceDetail";
import { useOverlayLoaderStore } from "./overlayLoader";

export const useAuthStore = defineStore("auth", () => {
  const localStorageUser = localStorage.getItem("user");
  const overlayLoaderStore = useOverlayLoaderStore();
  const localUser = ref<Employee>();
  if (localStorageUser) {
    localUser.value = JSON.parse(localStorageUser);
  }
  const atdStore = useAtdStore();
  const loadingStore = useLoadingStore();
  const messageStore = useMessageStore();
  const loginUserName = ref("");
  const loggedInEmployee = ref<Employee>({
    age: 0,
    name: "",
    surname: "",
    tel: "",
    role: "พนักงาน",
    username: "",
  });

  const loggedInEmployeeAttendaceDetail = ref<Attendance>({
    employee: {
      username: "",
      age: 0,
      name: "",
      surname: "",
      role: "",
      tel: "",
    },
    status: "Waiting",
    wage: 0,
    workOff: "",
    workStart: "",
  });

  async function resetLoggedInEmployee() {
    await delay(100);
    loggedInEmployee.value = {
      age: 0,
      name: "",
      surname: "",
      tel: "",
      role: "",
      username: "",
    };
    loggedInEmployeeAttendaceDetail.value = {
      employee: {
        username: "",
        age: 0,
        name: "",
        surname: "",
        role: "",
        tel: "",
      },
      status: "Waiting",
      wage: 0,
      workOff: "",
      workStart: "",
    };
  }

  function delay(ms: number) {
    return new Promise((resolve) => setTimeout(resolve, ms));
  }

  function getCurrentDateTime() {
    const currentDate = new Date();
    const logginDatetime = ref("");
    // Ignore these error. The code work just fine.
    const day = String(currentDate.getDate()).padStart(2, "0");
    const month = String(currentDate.getMonth() + 1).padStart(2, "0");
    const year = currentDate.getFullYear();
    const hours = String(currentDate.getHours()).padStart(2, "0");
    const minutes = String(currentDate.getMinutes()).padStart(2, "0");
    const seconds = String(currentDate.getSeconds()).padStart(2, "0");
    logginDatetime.value = `${day}/${month}/${year} ${hours}:${minutes}:${seconds}`;
    return (logginDatetime.value = `${day}/${month}/${year} ${hours}:${minutes}:${seconds}`);
  }

  function hourDiff(dt1: string, dt2: string) {
    const date1 = new Date(dt1);
    const date2 = new Date(dt2);
    // Ignore this error. The code work just fine.
    const hour = Math.abs(date2 - date1) / 36e5;
    return hour;
  }

  const login = async (userName: string, password: string): Promise<void> => {
    // localStorage.setItem("recordStatus", JSON.stringify("recording"));
    overlayLoaderStore.overlayLoader = true;
    try {
      const res = await auth.login(userName, password);
      const recordStatus = localStorage.getItem("recordStatus");
      const recordMsg = ref("");
      if (recordStatus) {
        recordMsg.value = JSON.parse(recordStatus);

        if (
          (res.data.employee.role != "พนักงาน" &&
            recordMsg.value == "recording") ||
          (res.data.employee.role != "พนักงาน" &&
            recordMsg.value == "not recording") ||
          (res.data.employee.role == "พนักงาน" &&
            recordMsg.value == "recording")
        ) {
          if (localUser.value && localUser.value.username == userName) {
            const attendanceData = localStorage.getItem("attendance");
            if (attendanceData) {
              const tempattendance = JSON.parse(attendanceData);
              loggedInEmployeeAttendaceDetail.value.employee =
                tempattendance.employee;
              loggedInEmployeeAttendaceDetail.value.status =
                tempattendance.status;
              loggedInEmployeeAttendaceDetail.value.wage = tempattendance.wage;
              loggedInEmployeeAttendaceDetail.value.workStart =
                tempattendance.workStart;
              loggedInEmployeeAttendaceDetail.value.workOff =
                tempattendance.workOff;
            }
            localStorage.setItem("user", JSON.stringify(res.data.employee));
            localStorage.setItem("token", res.data.access_token);
            loggedInEmployee.value = res.data.employee;
          } else {
            localStorage.setItem("user", JSON.stringify(res.data.employee));
            localStorage.setItem("token", res.data.access_token);
            loggedInEmployee.value = res.data.employee;
            loggedInEmployeeAttendaceDetail.value.employee =
              loggedInEmployee.value;
            loggedInEmployeeAttendaceDetail.value.workStart =
              getCurrentDateTime();
            atdStore.editedAttendaceDetail =
              loggedInEmployeeAttendaceDetail.value;
            localStorage.setItem(
              "attendance",
              JSON.stringify(loggedInEmployeeAttendaceDetail.value)
            );
          }
          loginUserName.value = userName;
          overlayLoaderStore.overlayLoader = false;
          router.push("/pointOfSell");
        } else if (
          recordMsg.value == "not recording" &&
          res.data.employee.role == "พนักงาน"
        ) {
          overlayLoaderStore.overlayLoader = false;
          messageStore.showMessage("Work time recording has not started yet");
        }
      }
    } catch (exception) {
      overlayLoaderStore.overlayLoader = false;
      loadingStore.closeDialog(0);
      console.log(exception);

      messageStore.showError("Syncing Data Failed!", exception.message);
    }
  };

  const logout = async () => {
    loggedInEmployeeAttendaceDetail.value.workOff = getCurrentDateTime();
    loggedInEmployeeAttendaceDetail.value.wage =
      hourDiff(
        loggedInEmployeeAttendaceDetail.value.workStart,
        loggedInEmployeeAttendaceDetail.value.workOff
      ) * atdStore.wageRate;
    loggedInEmployeeAttendaceDetail.value;
    atdStore.editedAttendaceDetail = loggedInEmployeeAttendaceDetail.value;
    await atdStore.addToDailyAttendanceDetails(atdStore.editedAttendaceDetail);
    resetLoggedInEmployee();
    localStorage.removeItem("user");
    localStorage.removeItem("token");
    localStorage.removeItem("attendance");
    router.replace("/login");
  };

  return {
    login,
    logout,
    loginUserName,
    loggedInEmployee,
    loggedInEmployeeAttendaceDetail,
    resetLoggedInEmployee,
  };
});
