import { computed, ref } from "vue";
import { defineStore } from "pinia";
import type Menu from "@/types/Menu";
import type OrderItems from "@/types/OrderItems";
import { useOrdersStore } from "./orders";
import { useMenuQueueStore } from "./menuQueue";

export const useCartStore = defineStore("cart", () => {
  const orderStore = useOrdersStore();
  const menuQueueStore = useMenuQueueStore();
  const totalQty = ref(0);
  const cart = ref<
    {
      menu: Menu;
      qty: number;
      price: number;
      sum: number;
    }[]
  >([]);
  const watingDialog = ref(false);
  const orderingDialog = ref(false);
  const cancelDialog = ref(false);
  const addedMenu = ref({
    menu: {
      name: "",
      imgSrc: "",
      category: "All",
      descript: "",
      size: "",
      price: 0,
      status: "Active",
      qty: 1,
    },
    status: "",
    qty: 1,
    price: 0,
    total: 0,
  });

  function getTotalQty(): number {
    return cart.value.reduce((total, item) => total + item.qty, 0);
  }

  const addMenu = (menu: OrderItems) => {
    // cart.value.push(menu);
  };

  const clear = (index: number) => {
    cart.value.splice(index, 1);
    totalQty.value = totalQty.value - 1;
    totalQty.value = getTotalQty();
  };

  const total = computed(() => {
    let sum = 0;
    for (const p of cart.value) {
      sum = sum + p.sum;
    }
    return sum;
  });

  const addToCart = (menu: Menu) => {
    const index = cart.value.findIndex((item) => item.menu.id == menu.id);
    if (index !== -1) {
      //found index
      cart.value[index].qty++;
      cart.value[index].sum = cart.value[index].qty * cart.value[index].price;
      totalQty.value = getTotalQty();
    } else {
      //not found index
      cart.value.push({
        menu: menu,
        qty: 1,
        price: menu.price,
        sum: 1 * menu.price,
      });
      totalQty.value = getTotalQty();
    }
  };

  function increment(index: number) {
    cart.value[index].qty++;
    cart.value[index].sum += cart.value[index].price;
    totalQty.value = getTotalQty();
  }
  function decrement(index: number) {
    if (cart.value[index].qty > 1) {
      cart.value[index].qty--;
      cart.value[index].sum -= cart.value[index].price;
    }
    totalQty.value = getTotalQty();
  }

  function checkOut(tableId: number, empId: number) {
    const odItem = cart.value.map(
      (item) =>
        <{ quantity: number; menuId: number }>{
          quantity: item.qty,
          menuId: item.menu.id,
        }
    );
    orderStore.openOrder(tableId, empId, odItem);
    menuQueueStore.getMenuQueue();
    cart.value = [];
    console.log(cart.value);
    totalQty.value = 0;
  }

  return {
    cart,
    addMenu,
    clear,
    watingDialog,
    addToCart,
    increment,
    decrement,
    total,
    orderingDialog,
    checkOut,
    cancelDialog,
    totalQty,
  };
});
