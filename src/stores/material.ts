import { ref } from "vue";
import { defineStore } from "pinia";
import type Material from "@/types/Material";
import materialService from "@/service/material";
import { useMessageStore } from "./message";
import { useLoadingStore } from "./loading";

export const useMaterialStore = defineStore("material", () => {
  const messageStore = useMessageStore();
  const loadingStore = useLoadingStore();
  const deleteDialog = ref(false);
  const materialDialog = ref(false);
  const updateDialog = ref(false);
  const oldValue = ref(0);
  const newValue = ref(0);
  const editedMaterial = ref<Material>({
    category: "เนื้อสด",
    name: "",
    minimum: 0,
    onhand: 0,
    unit: "",
  });
  const updatedMaterial = ref<Material>({
    category: "เนื้อสด",
    name: "",
    minimum: 0,
    onhand: 0,
    unit: "",
  });
  const materials = ref<Material[]>([]);

  const getMaterials = async () => {
    loadingStore.showWithMessage("Syncing Data");
    try {
      materials.value = (await materialService.getMaterials()).data;
    } catch (exception) {
      loadingStore.isLoading = false;
      // Ignore these error. The code work just fine.
      loadingStore.closeDialog(0);
      messageStore.showError("Syncing Data Failed!", exception.message);
    }
    loadingStore.closeDialog(0);
  };

  const saveMaterial = async () => {
    materialDialog.value = false;
    updateDialog.value = false;
    loadingStore.showWithMessage(
      editedMaterial.value.id ? "Saving in Progress" : "Adding in Progress"
    );
    if (newValue.value != 0) {
      editedMaterial.value.onhand = oldValue.value + newValue.value;
    }
    try {
      if (editedMaterial.value.id) {
        await materialService.updateMaterial(
          editedMaterial.value.id,
          editedMaterial.value
        );
      } else {
        await materialService.saveMaterial(editedMaterial.value);
      }
    } catch (exception) {
      loadingStore.closeDialog(0);
      // Ignore these error. The code work just fine.
      messageStore.showError(
        "Request Failed!",
        exception.response.data.message,
        exception.response.status
      );
    }
    await getMaterials();
    loadingStore.closeDialog(0);
    resetEditedMaterial();
  };

  const deleteMaterial = async () => {
    deleteDialog.value = false;
    loadingStore.showWithMessage("Deleting in Progress");
    try {
      await materialService.deleteMaterial(editedMaterial.value.id!);
      await getMaterials();
      resetEditedMaterial();
    } catch (exception) {
      loadingStore.closeDialog(0);
      // Ignore these error. The code work just fine.
      messageStore.showError(
        "Request Failed!",
        exception.response.data.message,
        exception.response.status
      );
    }
    loadingStore.closeDialog(0);
  };

  const resetEditedMaterial = async () => {
    await new Promise((resolve) => setTimeout(resolve, 100));
    editedMaterial.value = {
      category: "เนื้อสด",
      name: "",
      minimum: 0,
      onhand: 0,
      unit: "",
    };
    oldValue.value = 0;
    newValue.value = 0;
  };

  return {
    materials,
    deleteDialog,
    materialDialog,
    resetEditedMaterial,
    editedMaterial,
    getMaterials,
    saveMaterial,
    deleteMaterial,
    updateDialog,
    oldValue,
    newValue,
    updatedMaterial,
  };
});
