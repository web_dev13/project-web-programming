import { ref } from "vue";
import { defineStore } from "pinia";
import type ShowOrder from "@/types/ShowOrder";
import { useOrdersStore } from "./orders";

export const useShowOrderStore = defineStore("showOrder", () => {
  const showDeleteDialog = ref(false);
  const showEditDialog = ref(false);
  const showAddDialog = ref(false);
  const orderStore = useOrdersStore();
  let menuId = 4;
  // const addNew = ref("")
  const deleteMenuId = ref(-1);
  const editedMenu = ref<ShowOrder>({
    id: -1,
    table: "",
    name: [],
    setSize: [],
    price: [],
    qty: [],
  });
  const menu = ref<ShowOrder[]>([
    {
      id: 1,
      table: "T01",
      name: ["ส้มตำ", "ผัดไท", "ผัดคะน้า"],
      setSize: ["ปกติ", "พิเศษ", "พิเศษ"],
      price: [40, 50, 50],
      qty: [1, 1, 1],
    },
    {
      id: 2,
      table: "T02",
      name: ["ข้าวผัด"],
      setSize: ["ใหญ่"],
      price: [40],
      qty: [1],
    },
    {
      id: 3,
      table: "T03",
      name: ["ข้าวมันไก่ทอด", "ลาบเนื้อ"],
      setSize: ["ปกติ", "ปกติ"],
      price: [35, 40],
      qty: [1, 1],
    },
    {
      id: 4,
      table: "T04",
      name: ["ปลาราดพริก"],
      setSize: ["ปกติ"],
      price: [65],
      qty: [1],
    },
  ]);

  const clear = () => {
    editedMenu.value = {
      id: -1,
      table: "",
      name: [],
      setSize: [],
      price: [],
      qty: [],
    };
  };

  const editMenu = (menu: ShowOrder) => {
    editedMenu.value = { ...menu };
    showEditDialog.value = true;
  };

  const saveMenu = () => {
    if (editedMenu.value.id !== undefined && editedMenu.value.id < 0) {
      editedMenu.value.id = menuId++;
      menu.value.push(editedMenu.value);
    } else {
      const index = menu.value.findIndex(
        (item) => item.id === editedMenu.value.id
      );
      menu.value[index] = editedMenu.value;
    }
    showEditDialog.value = false;
    clear();
  };

  const deleteMenu = (table: string): void => {
    const index = menu.value.findIndex((item) => item.table === table);
    if (index !== -1) {
      menu.value.splice(index, 1);
    }
  };

  const deleteMenuItem = (table: string): void => {
    deleteMenu(table);
  };
  return {
    menu,
    deleteMenuItem,
    // deleteConfirm,
    showDeleteDialog,
    saveMenu,
    clear,
    editMenu,
    showEditDialog,
    editedMenu,
    showAddDialog,
  };
});
