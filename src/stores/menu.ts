import { ref } from "vue";
import { defineStore } from "pinia";
import type Menu from "@/types/Menu";
import type MenuQueue from "@/types/MenuQueue";
import menuService from "@/service/menu";
import { useMessageStore } from "./message";
import { useLoadingStore } from "./loading";

export const useMenuStore = defineStore("menu", () => {
  const loadingStore = useLoadingStore();
  const deleteMenuDialog = ref(false);
  const editMenuDialog = ref(false);
  const messageStore = useMessageStore();
  const editedMenu = ref<Menu & { files: File[] }>({
    name: "",
    imgSrc: "",
    category: "All",
    descript: "",
    size: "",
    price: 0,
    status: "Active",
    qty: 1,
    files: [],
  });

  const menus = ref<Menu[]>([]);
  const menuQueues = ref<MenuQueue[]>([]);

  const resetEditedMenu = async () => {
    await delay(100);
    editedMenu.value = {
      name: "",
      imgSrc: "default.png",
      category: "All",
      descript: "",
      size: "",
      price: 0,
      status: "Active",
      qty: 1,
      files: [],
    };
  };

  function delay(ms: number) {
    return new Promise((resolve) => setTimeout(resolve, ms));
  }

  async function getMenus() {
    loadingStore.showWithMessage("Syncing Data");
    try {
      const res = await menuService.getMenus();
      menus.value = res.data;
      menuQueues.value = [];
      for (const item of res.data) {
        if (item.menuQueues.length > 0) {
          for (const menuQueuesItem of item.menuQueues) {
            menuQueues.value.push(menuQueuesItem);
          }
        }
      }
    } catch (exception) {
      loadingStore.closeDialog(0);
      // Ignore these error. The code work just fine.
      messageStore.showError("Syncing Data Failed!", exception.message);
    }
    loadingStore.closeDialog(0);
  }

  async function saveMenu() {
    editMenuDialog.value = false;
    loadingStore.showWithMessage(
      editedMenu.value.id ? "Saving in Progress" : "Adding in Progress"
    );
    try {
      if (editedMenu.value.id) {
        await menuService.updateMenu(editedMenu.value.id, editedMenu.value);
      } else {
        await menuService.saveMenu(editedMenu.value);
      }
      await getMenus();
      resetEditedMenu();
    } catch (exception) {
      loadingStore.closeDialog(0);
      // Ignore these error. The code work just fine.
      messageStore.showError(
        "Request Failed!",
        exception.response.data.message,
        exception.response.status
      );
    }
    loadingStore.closeDialog(0);
  }

  async function deleteMenu() {
    deleteMenuDialog.value = false;
    loadingStore.showWithMessage("Deleting in Progress");
    try {
      await menuService.deleteMenu(editedMenu.value.id!);
      await getMenus();
      resetEditedMenu();
    } catch (exception) {
      loadingStore.closeDialog(0);
      // Ignore these error. The code work just fine.
      messageStore.showError(
        "Request Failed!",
        exception.response.data.message,
        exception.response.status
      );
    }
    loadingStore.closeDialog(0);
  }

  return {
    menus,
    deleteMenuDialog,
    resetEditedMenu,
    editMenuDialog,
    editedMenu,
    saveMenu,
    deleteMenu,
    getMenus,
    menuQueues,
  };
});
