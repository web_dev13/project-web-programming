import { defineStore } from "pinia";
import auth from "@/service/auth";
import { useLoadingStore } from "./loading";
import { useMessageStore } from "./message";
import router from "@/router";

export const useAuthStore = defineStore("auth", () => {
  const loadingStore = useLoadingStore();
  const messageStore = useMessageStore();

  const isLogin = () => {
    const user = localStorage.getItem("user");
    return user ? true : false;
  };

  const login = async (userName: string, password: string): Promise<void> => {
    loadingStore.isLoading = true;
    try {
      const res = await auth.login(userName, password);
      localStorage.setItem("user", JSON.stringify(res.data.user));
      localStorage.setItem("token", res.data.access_token);
      router.push("/");
    } catch (e) {
      loadingStore.closeDialog(0);
      messageStore.showMessage("Username หรือ Password ไม่ถูกต้อง");
    }
    loadingStore.closeDialog(0);
  };

  const logout = () => {
    localStorage.removeItem("user");
    localStorage.removeItem("token");
    router.replace("/login");
  };

  return { login, logout, isLogin };
});
