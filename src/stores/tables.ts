import { ref } from "vue";
import { defineStore } from "pinia";
import type Table from "@/types/Table";
import tableService from "@/service/table";
import { useMessageStore } from "./message";

export const useTableStore = defineStore("Table", () => {
  const messageStore = useMessageStore();
  const tables = ref<Table[]>([
    {
      id: 1,
      maxSeat: 1,
      status: "Active",
      qrCodeSource: "test.png",
    },
    {
      id: 2,
      maxSeat: 2,
      status: "Active",
      qrCodeSource: "test.png",
    },
    {
      id: 3,
      maxSeat: 2,
      status: "Non Active",
      qrCodeSource: "test.png",
    },
    {
      id: 4,
      maxSeat: 3,
      status: "Active",
      qrCodeSource: "test.png",
    },
    {
      id: 5,
      maxSeat: 1,
      status: "Non Active",
      qrCodeSource: "test.png",
    },
  ]);
  const showDialog = ref(false);
  // const showEditDialog = ref(false);
  // const showDeleteDialog = ref(false);
  const editedTable = ref<Table>({
    id: -1,
    maxSeat: 0,
    status: "",
    qrCodeSource: "",
  });
  const editedStatus = ref("");

  async function getTables() {
    try {
      const res = await tableService.getTables();
      tables.value = res.data;
    } catch (exception) {
      // Ignore these error. The code work just fine.
      messageStore.showError("Can't get the data from database!", exception);
    }
  }

  function changeTableStatus(table: Table, status: string) {
    editedTable.value = JSON.parse(JSON.stringify(table));
    if (status == "Avaliable") {
      showDialog.value = false;
      editedStatus.value = status;
      saveTable();
    } else {
      showDialog.value = true;
      editedStatus.value = status;
    }
  }

  async function saveTable() {
    editedTable.value.status = editedStatus.value;
    try {
      const res = await tableService.updateTable(
        editedTable.value.id!,
        editedTable.value
      );

      await getTables();
    } catch (exception) {
      // Ignore these error. The code work just fine.
      messageStore.showError("Error!", exception);
    }
    showDialog.value = false;
  }

  return {
    tables,
    getTables,
    showDialog,
    changeTableStatus,
    saveTable,
    editedTable,
  };
});
