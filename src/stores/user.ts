import { ref } from "vue";
import { defineStore } from "pinia";
import type User from "@/types/User";

export const useUserStore = defineStore("user", () => {
  let lastId = 4;
  const isTable = ref(true);
  const dialog = ref(false);
  const editedUser = ref<User>({ id: -1, login: "", name: "", password: "" });
  const users = ref<User[]>([
    { id: 1, login: "admin", name: "Administrator", password: "Pass@1234" },
    { id: 2, login: "user1", name: "User 1", password: "Pass@1234" },
    { id: 3, login: "user2", name: "User 2", password: "Pass@1234" },
  ]);

  const login = (loginName: string, password: string): boolean =>
    users.value.some(
      (item) => item.login === loginName && item.password === password
    );

  const deleteUser = (id: number): void =>
    users.value.splice(
      users.value.findIndex((item) => item.id === id),
      1
    );

  const saveUser = (): void => {
    if (editedUser.value.id! < 0) {
      editedUser.value.id = lastId++;
      users.value.push(editedUser.value);
    } else {
      const index = users.value.findIndex(
        (item) => item.id === editedUser.value.id
      );
      users.value[index] = editedUser.value;
    }
    dialog.value = false;
    clear();
  };

  const editUser = (user: User): void => {
    editedUser.value = { ...user };
    dialog.value = true;
  };

  const clear = (): void => {
    editedUser.value = { id: -1, login: "", name: "", password: "" };
  };

  return {
    users,
    isTable,
    dialog,
    editedUser,
    login,
    deleteUser,
    saveUser,
    editUser,
    clear,
  };
});
