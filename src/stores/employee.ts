import { ref } from "vue";
import { defineStore } from "pinia";
import type Employee from "@/types/Employee";
import employeeService from "@/service/employee";
import { useMessageStore } from "./message";
import { useLoadingStore } from "./loading";

export const useEmployeeStore = defineStore("employees", () => {
  const loadingStore = useLoadingStore();
  const messageStore = useMessageStore();
  const employees = ref<Employee[]>([]);
  const addEmployeeDialog = ref(false);
  const deleteEmployeeDialog = ref(false);
  const editedEmployee = ref<Employee>({
    age: 0,
    name: "",
    surname: "",
    tel: "",
    role: "พนักงาน",
    username: "",
  });

  async function getEmployees() {
    loadingStore.showWithMessage("Syncing Data");
    try {
      const response = await employeeService.getEmployee();
      employees.value = response.data;
    } catch (exception) {
      loadingStore.closeDialog(0);
      // Ignore these error. The code work just fine.
      messageStore.showError("Syncing Data Failed!", exception.message);
    }
    loadingStore.closeDialog(0);
  }

  async function saveEmployee() {
    addEmployeeDialog.value = false;
    loadingStore.showWithMessage(
      editedEmployee.value.id ? "Saving in Progress" : "Adding in Progress"
    );
    try {
      if (editedEmployee.value.id) {
        await employeeService.updateEmployee(
          editedEmployee.value.id,
          editedEmployee.value
        );
      } else {
        await employeeService.saveEmployee(editedEmployee.value);
      }
    } catch (exception) {
      loadingStore.closeDialog(0);
      console.log(exception.response.data.message);

      // Ignore these error. The code work just fine.
      messageStore.showError(
        "Request Failed!",
        exception.response.data.message,
        exception.response.status
      );
    }
    await getEmployees();
    resetEditedEmployee();
    loadingStore.closeDialog(0);
  }

  async function deleteEmployee() {
    deleteEmployeeDialog.value = false;
    loadingStore.showWithMessage("Deleting in Progress");
    try {
      await employeeService.deleteEmployee(editedEmployee.value.id!);
      await getEmployees();
      resetEditedEmployee();
    } catch (exception) {
      loadingStore.closeDialog(0);
      // Ignore these error. The code work just fine.
      messageStore.showError(
        "Request Failed!",
        exception.response.data.message,
        exception.response.status
      );
    }
    loadingStore.closeDialog(0);
  }

  async function resetEditedEmployee() {
    await delay(100);
    editedEmployee.value = {
      age: 0,
      name: "",
      surname: "",
      tel: "",
      role: "พนักงาน",
      username: "",
    };
  }

  function delay(ms: number) {
    return new Promise((resolve) => setTimeout(resolve, ms));
  }

  return {
    getEmployees,
    employees,
    editedEmployee,
    addEmployeeDialog,
    deleteEmployeeDialog,
    saveEmployee,
    deleteEmployee,
    resetEditedEmployee,
  };
});
