import { ref } from "vue";
import { defineStore } from "pinia";
import type Stock from "@/types/Stock";
import { useLoadingStore } from "./loading";

export const useStockStore = defineStore("stock", () => {
  const loadingStore = useLoadingStore();
  const showDeleteDialog = ref(false);
  const showEditDialog = ref(false);
  const showAddDialog = ref(false);
  const showUpdateDialog = ref(false);
  const sumOnhand = ref(0);
  let stockId = 6;
  const deleteStockId = ref(-1);
  const updatedStock = ref<Stock>({
    categories: "",
    name: "",
    minimum: 0,
    onHand: 0,
    unit: "",
  });
  const editedStock = ref<Stock>({
    categories: "",
    name: "",
    minimum: 0,
    onHand: 0,
    unit: "",
  });
  const stocks = ref<Stock[]>([
    {
      id: 1,
      categories: "เครื่องปรุง",
      name: "น้ำตาลทราย",
      minimum: 100,
      onHand: 300,
      unit: "กรัม",
    },
    {
      id: 2,
      categories: "เครื่องปรุง",
      name: "น้ำปลา",
      minimum: 2,
      onHand: 4,
      unit: "แกลลอน",
    },
    {
      id: 3,
      categories: "เนื้อสด",
      name: "หมูสับ",
      minimum: 2,
      onHand: 5,
      unit: "กิโลกรัม",
    },
    {
      id: 4,
      categories: "เนื้อสด",
      name: "ไก่สด",
      minimum: 2,
      onHand: 5,
      unit: "กิโลกรัม",
    },
    {
      id: 5,
      categories: "เครื่องปรุง",
      name: "น้ำปลาร้า",
      minimum: 2,
      onHand: 3,
      unit: "แกลลอน",
    },
  ]);

  const clear = () => {
    editedStock.value = {
      categories: "",
      name: "",
      minimum: 0,
      onHand: 0,
      unit: "",
    };
  };

  const editstock = (stock: Stock) => {
    editedStock.value = { ...stock };
    showEditDialog.value = true;
  };
  const updateStock = (stock: Stock) => {
    //updatedStock.value = { ...stock };
    //editedStock.value = { ...stock };
    editedStock.value = JSON.parse(JSON.stringify(stock));
    showUpdateDialog.value = true;
  };

  const saveStock = () => {
    if (editedStock.value.id! < 0) {
      editedStock.value.id = stockId++;
      stocks.value.push(editedStock.value);
    } else {
      const index = stocks.value.findIndex(
        (item) => item.id === editedStock.value.id
      );
      stocks.value[index] = editedStock.value;
    }
    showEditDialog.value = false;
    clear();
  };
  const saveUpdateStock = (num: number) => {
    editedStock.value.onHand += num;
    showUpdateDialog.value = false;
    clear();
  };

  const deleteStock = (id: number): void => {
    showDeleteDialog.value = false;
    const index = stocks.value.findIndex(
      (item) => item.id === deleteStockId.value
    );
    stocks.value.splice(index, 1);
  };

  const deleteConfirm = (id: number) => {
    showDeleteDialog.value = true;
    deleteStockId.value = id;
  };

  return {
    stocks,
    deleteStock,
    deleteConfirm,
    showDeleteDialog,
    showEditDialog,
    saveStock,
    clear,
    editstock,
    editedStock,
    showAddDialog,
    updatedStock,
    updateStock,
    showUpdateDialog,
    saveUpdateStock,
    sumOnhand,
  };
});
