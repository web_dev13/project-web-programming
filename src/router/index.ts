import { createRouter, createWebHistory } from "vue-router";
import HomeView from "../views/HomeView.vue";
import LoginView from "@/views/LoginView.vue";

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: "/",
      name: "home",
      components: {
        default: HomeView,
        menu: () => import("@/components/navigator/MainMenu.vue"),
        header: () => import("@/components/header/MainHeader.vue"),
      },
      meta: {
        layout: "MainView",
        requiresAuth: true,
      },
    },
    {
      path: "/about",
      name: "about",
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import("../views/AboutView.vue"),
    },
    {
      path: "/login",
      name: "login",
      components: {
        default: LoginView,
      },
      meta: {
        layout: "FullLayout",
      },
    },
    {
      path: "/user",
      name: "user",

      components: {
        default: () => import("../views/user/UserView.vue"),
        menu: () => import("@/components/navigator/MainMenu.vue"),
        header: () => import("@/components/header/MainHeader.vue"),
      },
      meta: {
        layout: "MainView",
      },
    },

    {
      path: "/demoServe",
      name: "demoServe",
      components: {
        default: () => import("../views/DemoServe/ServingView.vue"),
        menu: () => import("@/components/navigator/MainMenu.vue"),
        header: () => import("@/components/header/MainHeader.vue"),
      },
      meta: {
        layout: "MainView",
        requiresAuth: true,
      },
    },
    {
      path: "/demoEmp",
      name: "demoEmp",
      components: {
        default: () => import("../views/DemoEmployee/EmployeeView.vue"),
        menu: () => import("@/components/navigator/MainMenu.vue"),
        header: () => import("@/components/header/MainHeader.vue"),
      },
      meta: {
        layout: "MainView",
        requiresAuth: true,
      },
    },
    {
      path: "/demoMenu",
      name: "demoMenu",
      components: {
        default: () => import("../views/DemoMenu/MainMenuView.vue"),
        menu: () => import("@/components/navigator/MainMenu.vue"),
        header: () => import("@/components/header/MainHeader.vue"),
      },
      meta: {
        layout: "MainView",
        requiresAuth: true,
      },
    },
    {
      path: "/demoClearTable",
      name: "demoClearTable",
      components: {
        default: () => import("../views/DemoClearTable/ClearTableView.vue"),
        menu: () => import("@/components/navigator/MainMenu.vue"),
        header: () => import("@/components/header/MainHeader.vue"),
      },
      meta: {
        layout: "MainView",
        requiresAuth: true,
      },
    },
    {
      path: "/demoOrder",
      name: "demoOrder",
      components: {
        default: () => import("../views/DemoOrder/OrderList.vue"),
        menu: () => import("@/components/navigator/MainMenu.vue"),
        header: () => import("@/components/header/MainHeader.vue"),
      },
      meta: {
        layout: "MainView",
        requiresAuth: true,
      },
    },
    {
      path: "/demoFoodQueue",
      name: "demoFoodQueue",
      components: {
        default: () => import("../views/DemoFood/QueueView.vue"),
        menu: () => import("@/components/navigator/MainMenu.vue"),
        header: () => import("@/components/header/MainHeader.vue"),
      },
      meta: {
        layout: "MainView",
        requiresAuth: true,
      },
    },
    {
      path: "/demoTableStatus",
      name: "demoTableStatus",
      components: {
        default: () => import("../views/DemoTableStatus/TableStatusView.vue"),
        menu: () => import("@/components/navigator/MainMenu.vue"),
        header: () => import("@/components/header/MainHeader.vue"),
      },
      meta: {
        layout: "MainView",
        requiresAuth: true,
      },
    },
    {
      path: "/demoTable",
      name: "demoTable",
      components: {
        default: () => import("../views/DemoTable/TableManagementView.vue"),
        menu: () => import("@/components/navigator/MainMenu.vue"),
        header: () => import("@/components/header/MainHeader.vue"),
      },
      meta: {
        layout: "MainView",
        requiresAuth: true,
      },
    },
    {
      path: "/demoClearTable",
      name: "demoClearTable",
      components: {
        default: () => import("../views/DemoTable/ChangeTableStatusView.vue"),
        menu: () => import("@/components/navigator/MainMenu.vue"),
        header: () => import("@/components/header/MainHeader.vue"),
      },
      meta: {
        layout: "MainView",
        requiresAuth: true,
      },
    },
    {
      path: "/demoStock",
      name: "demoStock",
      components: {
        default: () => import("../views/DemoStock/MainMaterialView.vue"),
        menu: () => import("@/components/navigator/MainMenu.vue"),
        header: () => import("@/components/header/MainHeader.vue"),
      },
      meta: {
        layout: "MainView",
        requiresAuth: true,
      },
    },

    {
      path: "/pointOfSell",
      name: "pointOfSell",
      components: {
        default: () => import("../views/PointOfSell/PointOfSellView.vue"),
        menu: () => import("@/components/navigator/MainMenu.vue"),
        header: () => import("@/components/header/MainHeader.vue"),
      },
      meta: {
        layout: "MainView",
      },
    },
    {
      path: "/mainMenu/:tableId/:employeeId",
      name: "mainMenu",
      components: {
        default: () => import("../views/Cutomer/MainMenuView.vue"),
        // menu: () => import("@/components/navigator/MainMenu.vue"),
        header: () => import("@/components/header/MainCustomer.vue"),
      },
      meta: {
        layout: "MainMenu",
      },
    },
    {
      path: "/mainMenuitem/:tableId/:employeeId",
      name: "mainMenuitem",
      components: {
        default: () => import("../views/Cutomer/MenuItem.vue"),
        // menu: () => import("@/components/navigator/MainMenu.vue"),
        header: () => import("@/components/header/MainCustomer.vue"),
      },
      meta: {
        layout: "MainMenu",
      },
    },
    {
      path: "/demoAttendance",
      name: "demoAttendance",
      components: {
        default: () => import("../views/DemoAttendance/MainAttendance.vue"),
        menu: () => import("@/components/navigator/MainMenu.vue"),
        header: () => import("@/components/header/MainHeader.vue"),
      },
      meta: {
        layout: "MainView",
        requiresAuth: true,
      },
    },
  ],
});

function isLoggedIn() {
  const user = localStorage.getItem("user");
  if (user) {
    return true;
  } else {
    return false;
  }
}

router.beforeEach((to, from) => {
  if (to.meta.requiresAuth && !isLoggedIn()) {
    return {
      path: "/login",
      query: { redirect: to.fullPath },
    };
  }
});

export default router;
