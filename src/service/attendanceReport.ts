import http from "@/service/axios";
import type AttendanceReport from "@/types/AttendanceReport";

function getAttendanceReports() {
  return http.get("/attendance-reports");
}

function getAttendanceReportById(id: number) {
  return http.get(`/attendance-reports/id/${id}`);
}

function getAttendanceReportByDate(date: string) {
  return http.get(`/attendance-reports/date/${date}`);
}

function saveAttendanceReport(attendanceReport: AttendanceReport) {
  return http.post("/attendance-reports", attendanceReport);
}

function updateAttendanceReport(
  id: number,
  attendanceReport: AttendanceReport
) {
  return http.patch(`/attendance-reports/${id}`, attendanceReport);
}

function deleteAttendanceReport(id: number) {
  return http.delete(`/attendance-reports/${id}`);
}

export default {
  getAttendanceReports,
  getAttendanceReportById,
  getAttendanceReportByDate,
  updateAttendanceReport,
  saveAttendanceReport,
  deleteAttendanceReport,
};
