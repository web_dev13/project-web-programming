import http from "@/service/axios";
import type Orders from "@/types/Orders";

function getOrders() {
  return http.get("/orders");
}
function getOrder(id: number) {
  return http.get(`/orders/${id}`);
}
function saveOrder(order: {
  storeName: string;
  status: string;
  tax: number;
  paymentType: string;
  cash: number;
  change: number;
  tableId: number;
  employeeId: number;
  orderItems: { quantity: number; menuId: number }[];
}) {
  return http.post("/orders", order);
}

function updateOrder(id: number, orders: Orders) {
  return http.patch(`/orders/${id}`, orders);
}
function updateOrderPay(id: number, orders: Orders) {
  return http.patch(`/orders/pay/${id}`, orders);
}

function deleteOrder(id: number) {
  return http.delete(`/orders/${id}`);
}

export default {
  getOrders,
  deleteOrder,
  saveOrder,
  updateOrder,
  getOrder,
  updateOrderPay,
};
