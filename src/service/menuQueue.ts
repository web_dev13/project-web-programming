import http from "@/service/axios";
import type MenuQueue from "@/types/MenuQueue";

function getMenuQueue() {
  return http.get("/menu-queue");
}
function getMenuQueueWithODItem(odItemId: number) {
  return http.get(`menu-queue/orderItem/${odItemId}`);
}

function saveMenuQueue() {
  return "unavailable";
}

function updateMenuQueue(id: number, menuQueue: MenuQueue) {
  return http.patch(`/menu-queue/${id}`, menuQueue);
}
function updateODItem(id: number, menuQueue: MenuQueue) {
  return http.patch(`/menu-queue/orderItem/${id}`, menuQueue);
}

function deleteMenuQueue(id: number) {
  return http.delete(`/menu-queue/${id}`);
}

export default {
  getMenuQueue,
  updateMenuQueue,
  saveMenuQueue,
  deleteMenuQueue,
  getMenuQueueWithODItem,
  updateODItem,
};
