import router from "@/router";
import axios from "axios";

const instance = axios.create({
  baseURL: "http://localhost:3000",
});

instance.interceptors.request.use(
  async function (config) {
    const token = localStorage.getItem("token");
    if (token) {
      config.headers.Authorization = `Bearer ${token}`;
    }
    return config;
  },
  function (error) {
    return Promise.reject(error);
  }
);

// instance.interceptors.response.use(
//   async function (res) {
//     return res;
//   },
//   function (error) {
//     if (401 === error.response.status) {
//       router.replace("/login");
//     }
//     return Promise.reject(error);
//   }
// );

export default instance;
