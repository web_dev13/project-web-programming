import http from "@/service/axios";
import type AttendanceDetail from "@/types/AttendanceDetail";

function getAttendanceDetails() {
  return http.get("/attendanceDetails");
}

function getAttendanceDetailById(id: number) {
  return http.get(`/attendanceDetails/${id}`);
}

function saveAttendanceDetail(attendanceDetail: AttendanceDetail) {
  return http.post("/attendanceDetails", attendanceDetail);
}

function updateAttendanceDetail(
  id: number,
  attendanceDetail: AttendanceDetail
) {
  return http.patch(`/attendanceDetails/${id}`, attendanceDetail);
}

function deleteAttendanceDetail(id: number) {
  return http.delete(`/attendanceDetails/${id}`);
}

export default {
  getAttendanceDetails,
  getAttendanceDetailById,
  updateAttendanceDetail,
  saveAttendanceDetail,
  deleteAttendanceDetail,
};
